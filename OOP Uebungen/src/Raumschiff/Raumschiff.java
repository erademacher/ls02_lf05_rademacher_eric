package Raumschiff;

import java.util.ArrayList;

public class Raumschiff extends Ladung {

	private String schiffsName;
	private int photonenTorpedoAnzahl;
	private int energieVersorgungInProzent;
	private int zustandSchildeInProzent;
	private int zustandHuelleInProzent;
	private int zustandLebenserhaltungsSystemeInprozent;
	private int anzahlDroiden;
	private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsVerzeichnis = new ArrayList<Ladung>();
	private ArrayList<String> logbuch = new ArrayList<String>();
	private int zustandRaumschiff; 
	private String meldung;

	// Konstruktoren

	public Raumschiff() {
		super();
		

	}

	public Raumschiff(int photonentorpedoAnzahl, int energieVersorgungInProzent, int zustandSchildeInProzent,
			int zustandHuelleInProzent, int zustandLebenserhaltungsSystemeInProzent, int anzahlDroiden,
			String schiffsName, String ladung, int menge) {
		super(ladung,menge);
		
		this.photonenTorpedoAnzahl = photonentorpedoAnzahl;
		this.energieVersorgungInProzent = energieVersorgungInProzent;
		this.zustandSchildeInProzent = zustandSchildeInProzent;
		this.zustandHuelleInProzent = zustandHuelleInProzent;
		this.zustandLebenserhaltungsSystemeInprozent = zustandLebenserhaltungsSystemeInProzent;
		this.anzahlDroiden = anzahlDroiden;
		this.schiffsName = schiffsName;
	}

	// Methoden

	public int getPhotonenTorpedoAnzahl() {
		return this.photonenTorpedoAnzahl;
	}

	public void setPhotonenTorpedoAnzahl(int photonenTorpedoAnzahl) {
		this.photonenTorpedoAnzahl = photonenTorpedoAnzahl;
	}

	public int getEnergieVersorgungInProzent() {
		return energieVersorgungInProzent;
	}

	public void setEnergieVersorgungInProzent(int energieVersorgungInProzent) {
		this.energieVersorgungInProzent = energieVersorgungInProzent;
	}

	public String getSchiffsName() {
		return schiffsName;
	}

	public void setSchiffsName(String schiffsName) {
		this.schiffsName = schiffsName;
	}

	public int getZustandSchildeInProzent() {
		return zustandSchildeInProzent;
	}

	public void setZustandSchildeInProzent(int zustandSchildeInProzent) {
		this.zustandSchildeInProzent = zustandSchildeInProzent;
	}

	public int getZustandHuelleInProzent() {
		return zustandHuelleInProzent;
	}

	public void setZustandHuelleInProzent(int zustandHuelleInProzent) {
		this.zustandHuelleInProzent = zustandHuelleInProzent;
	}

	public int getZustandLebenserhaltungsSystemeInprozent() {
		return zustandLebenserhaltungsSystemeInprozent;
	}

	public void setZustandLebenserhaltungsSystemeInprozent(int zustandLebenserhaltungsSystemeInprozent) {
		this.zustandLebenserhaltungsSystemeInprozent = zustandLebenserhaltungsSystemeInprozent;
	}

	public int getAnzahlDroiden() {
		return anzahlDroiden;
	}

	public void setAnzahlDroiden(int anzahlDroiden) {
		this.anzahlDroiden = anzahlDroiden;
	}

	public void ladungHinzufuegen(Ladung A) {
		this.ladungsVerzeichnis.add(A);
	}
	
	public void ladungLoeschen(Ladung B) {
		this.ladungsVerzeichnis.remove(B);
	}

	public void photonenTorpedosLaden() {

	}
	
	public void photonenTorpedosSchiessen(Raumschiff Gegner_Schiff, Raumschiff Schiff) { 
		if(this.getPhotonenTorpedoAnzahl() == 0) {
			System.out.println("-=*Click*=-");
		}else {
			System.out.println("Photonentorpedo abgeschossen");
			photonenTorpedoAnzahl--;
			this.treffer(Gegner_Schiff);
		}
	}
	
	public void phaserkanoneSchiessen(Raumschiff Gegner_Schiff, Raumschiff Schiff) {
		if(this.getEnergieVersorgungInProzent() == 50) {
			System.out.println("-=*Click*=-");
		}else {
			System.out.println("Phaserkanone abgeschossen");
			this.setEnergieVersorgungInProzent((this.getEnergieVersorgungInProzent()-50));
			this.treffer(Gegner_Schiff);
		}
		
	}

	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}

	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}
		
	public void nachrichtAnAlle(String message) {
		
	}
	
	public ArrayList<String> eintraegeLogbuchzurueckgeben(){
		return broadcastKommunikator;
	}
		
	public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle, int anzahlDroiden) {
		
	}
		
	
	public boolean ladungsVerzeichnisAufraeumen() {
		return ladungsVerzeichnisAufraeumen();
		
	}
	
	public void ladungsVerzeichnisAusgabe() {
		
	}

	public ArrayList<String> getLogbuch() {
		return logbuch;
	}

	public void setLogbuch(ArrayList<String> logbuch) {
		this.logbuch = logbuch;
	}


	public int getZustandRaumschiff() {
		return zustandRaumschiff;
	}

	public void treffer(Raumschiff Schiff) {
		System.out.println(Schiff.getSchiffsName() +" wurde getroffen");
	}
	
	public void nachricht(String nachricht) {
		this.meldung = "Nachricht an Alle: " + nachricht;
		System.out.println(meldung);
	}
	
	public void doZustand() {
		this.meldung ="Das ist das Raumschiff " + this.getSchiffsName() + " und hat " + this.getPhotonenTorpedoAnzahl() + " Photonentorpedos am Board, ihre Energie ist momentan bei " + this.getEnergieVersorgungInProzent() + "% und die schildepunkte sind bei " + this.getZustandSchildeInProzent() + "% und die h�lle ist bei " + this.getZustandHuelleInProzent() + "%." + getLadungsVerzeichnis();
		System.out.println(meldung);
	}


	
	
}
